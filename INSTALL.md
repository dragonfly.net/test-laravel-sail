# prepare
export PHP_VERSION=8.1
sudo apt-get install -yq --no-install-recommends       git       redis       php${PHP_VERSION}-fpm       php${PHP_VERSION}-mysql       php${PHP_VERSION}-zip       php${PHP_VERSION}-xml              php${PHP_VERSION}-redis       php${PHP_VERSION}-readline       php${PHP_VERSION}-mongodb       php${PHP_VERSION}-curl       php${PHP_VERSION}-imagick       php${PHP_VERSION}-mbstring       php${PHP_VERSION}-bcmath       php${PHP_VERSION}-ssh2       php${PHP_VERSION}-gd       php${PHP_VERSION}-yaml
composer create-project laravel/laravel test-laravel-sail
cd test*
composer install
php artisan key:generate

# run
php artisan serve
curl http://127.0.0.1:8000/

# sail
# https://laravel.com/docs/10.x/sail
composer require laravel/sail --dev
php artisan sail:install
php artisan sail:add

# run docker
alias sail='bash vendor/bin/sail'
sail up -d
curl 127.0.0.1

# publish
sail artisan sail:publish

./vendor/bin/sail up

